//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>

class MidiMessage
{
public:
    MidiMessage()
    {
        number = 60;
        amplitude = 0.7;
    }
    MidiMessage(int initialNumber, int initialAmplitude)
    {
        number = initialNumber;
        amplitude = initialAmplitude;
    }
    ~MidiMessage()
    {
        
    }
    float getMidiNoteInHertz() const
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
    void setNoteNumber(int value)
    {
        if(value > 0)
        {
            number = value;
        }
    }
    int getNoteNumber() const
    {
        return number;
    }
    void setFloatVelocity(int velocity)
    {
        amplitude = velocity / 127.000;
    }
    float getFloatVelocity() const
    {
        return amplitude;
    }
    
private:
    int number;
    float amplitude;
};

int main (int argc, const char* argv[])
{
    MidiMessage note;
    
    /* displays value of initial data member 'note' */
    std::cout << "Note number:\t" << note.getNoteNumber() << "\n";
    std::cout << "Note frequency:\t" << note.getMidiNoteInHertz() << "\n";
    std::cout << "Note amplitude:\t" << note.getFloatVelocity() << "\n\n";
    
    /* collects new values for data member 'note' */
    int tempNote;
    std::cout << "Please enter new note number:\t";
    std::cin >> tempNote;
    note.setNoteNumber(tempNote);
    float tempVelocity;
    std::cout << "please enter velocity:\t";
    std::cin >> tempVelocity;
    note.setFloatVelocity(tempVelocity);
    std::cout << "\n";
    
    /* displays new values of data member 'note' */
    if(tempNote < 0)
    {
        std::cout << "INVALID NOTE NUMBER!\n\n";
    }
    else
    {
        std::cout << "Note number:\t" << note.getNoteNumber() << "\n";
        std::cout << "Note frequency:\t" << note.getMidiNoteInHertz() << "\n";
    }
    
    if(tempVelocity <= 127.000)
    {
        std::cout << "Note amplitude:\t" << note.getFloatVelocity() << "\n\n";
    }
    else
    {
        std::cout << "INVALID VELOCITY\n\n";
    }
    
    /* collects and displays values for new data member 'note1' */
    MidiMessage note1(64, 89);
    std::cout << "Note 1 number:\t" << note1.getNoteNumber() << "\n";
    std::cout << "Note 1 frequency:\t" << note1.getMidiNoteInHertz() << "\n";
    std::cout << "Note 1 amplitude:\t" << note1.getFloatVelocity() << "\n";

    return 0;
}

